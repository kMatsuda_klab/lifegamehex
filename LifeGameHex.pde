HexCells hexcells;

void setup(){
	size(1000, 1000);
	hexcells = new HexCells(50);
	frameRate(2);
}

void draw(){
	background(255);
	hexcells.calcNext();
	hexcells.update();
	hexcells.display();
}