class HexCell{
	int x, y;
	int state;
	int nextState;

	HexCell[] neighbours = new HexCell[6];

	HexCell(int _x, int _y, int _state){
		x = _x;
		y = _y;
		state = _state;
		nextState = 0;
	}

	void setNeighbours(HexCell c0, HexCell c1, HexCell c2, HexCell c3, HexCell c4, HexCell c5){
		neighbours[0] = c0;
		neighbours[1] = c1;
		neighbours[2] = c2;
		neighbours[3] = c3;
		neighbours[4] = c4;
		neighbours[5] = c5;
	}

	void calcNext(){
		int total_state = 0;
		for(int i = 0; i < 6; ++i){
			total_state += neighbours[i].state;
		}

		if(total_state == 0){
			nextState = 0;
		}else if(total_state < 3){
			if(random(1) < 1.0){
				nextState = 1;
			}else{
				nextState = 0;
			}
		}else{
			nextState = 0;
		}
	}

	void update(){
		state = nextState;
		nextState = 0;
	}

	void displayHex(float cellSize){
		if(state == 0){
			fill(255);
		}else if(state == 1){
			fill(0);
		}else{
			fill(100);
		}

		strokeWeight(2);
		pushMatrix();
		translate(2*cellSize + cellSize*x*(1+cos(PI/3)), 2*cellSize + 2*cellSize*y*sin(PI/3) + cellSize*sin(PI/3)*pow(-1, x+1)/2);
		beginShape();
		for(int i = 0; i < 6; ++i){
			vertex(cellSize*cos(PI*i/3), cellSize*sin(PI*i/3));
		}
		endShape(CLOSE);
		popMatrix();
	}
};