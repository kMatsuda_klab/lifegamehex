class HexCells{
	int num;
	HexCell[][] cells;

	HexCells(int _num){
		num = _num;
		cells = new HexCell[num][num];
		for(int j = 0; j < num; ++j){
			for(int i = 0; i < num; ++i){
				cells[i][j] = new HexCell(i, j, 0);
			}
		}

		cells[num/2][num/2].state = 1;

		for(int j = 0; j < num; ++j){
			for(int i = 0; i < num; ++i){
				int i_plus = (i+1)%num;
				int j_plus = (j+1)%num;
				int i_minus = (i+num-1)%num;
				int j_minus = (j+num-1)%num;

				if((i%2) == 0){
					cells[i][j].setNeighbours(cells[i][j_minus], cells[i_minus][j_minus], cells[i_minus][j], cells[i][j_plus], cells[i_plus][j], cells[i_plus][j_minus]);
				}else{
					cells[i][j].setNeighbours(cells[i][j_minus], cells[i_minus][j], cells[i_minus][j_plus], cells[i][j_plus], cells[i_plus][j_plus], cells[i_plus][j]);
				}

			}
		}



	}

	void calcNext(){
		for(int j = 0; j < num; ++j){
			for(int i = 0; i < num; ++i){
				cells[i][j].calcNext();
			}
		}
	}


	void update(){
		int total_state = 0;

		for(int j = 0; j < num; ++j){
			for(int i = 0; i < num; ++i){
				total_state += cells[i][j].state;
			}
		}
		println(total_state);


		for(int j = 0; j < num; ++j){
			for(int i = 0; i < num; ++i){
				cells[i][j].update();
			}
		}
	}


	void display(){
		for(int j = 0; j < num; ++j){
			for(int i = 0; i < num; ++i){
				cells[i][j].displayHex(height/num/2);
			}
		}
	}
}